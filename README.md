Postfix: mail transfer agent
=========
Installs postfix with default parameters (such as "Internet Server").  
You can add you own templates and config files.  
Remember: predefined config files will be replaced by files from templates with the same filenames.  

Include role
------------
```yaml
- name: postfix  
  src: https://gitlab.com/ansible_roles_v/postfix/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - postfix
```